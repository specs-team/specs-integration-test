package eu.specs.project.integrationtest.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class AppConfig {

    private String slaManagerApiAddress;
    private String serviceManagerApiAddress;
    private String sloManagerAddress;
    private String planningApiAddress;
    private String implementationApiAddress;
    private List<String> securityMechanismFiles;
    private String slaTemplateFileName;
    private int acceptedOfferNum;

    public AppConfig(String propertiesFile) throws IOException {
        Properties properties = new Properties();
        properties.load(this.getClass().getResourceAsStream(propertiesFile));
        this.slaManagerApiAddress = properties.getProperty("sla-manager-api.address");
        this.serviceManagerApiAddress = properties.getProperty("service-manager-api.address");
        this.sloManagerAddress = properties.getProperty("slo-manager-api.address");
        this.planningApiAddress = properties.getProperty("planning-api.address");
        this.implementationApiAddress = properties.getProperty("implementation-api.address");
        this.securityMechanismFiles = Arrays.asList(properties.getProperty("securityMechanisms.files").split(","));
        this.slaTemplateFileName = properties.getProperty("slaTemplate.fileName");
        this.acceptedOfferNum = Integer.parseInt(properties.getProperty("acceptedOfferNum"));
    }

    public String getSlaManagerApiAddress() {
        return slaManagerApiAddress;
    }

    public String getServiceManagerApiAddress() {
        return serviceManagerApiAddress;
    }

    public String getSloManagerAddress() {
        return sloManagerAddress;
    }

    public String getPlanningApiAddress() {
        return planningApiAddress;
    }

    public String getImplementationApiAddress() {
        return implementationApiAddress;
    }

    public List<String> getSecurityMechanismFiles() {
        return securityMechanismFiles;
    }

    public String getSlaTemplateFileName() {
        return slaTemplateFileName;
    }

    public int getAcceptedOfferNum() {
        return acceptedOfferNum;
    }
}
