package eu.specs.project.integrationtest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.utils.JsonDumper;
import eu.specs.project.negotiation.scm.SupplyChainManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlanningTestApp {
    private static final Logger logger = LogManager.getLogger(PlanningTestApp.class);
    private static AppConfig appConfig;

    public static void main(String[] args) throws Exception {
        appConfig = new AppConfig("/app.properties");
        new PlanningTestApp().start();
    }

    public void start() throws Exception {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                        //.register(new LoggingFilter())
                .build();
        WebTarget planningApiTarget = client.target(appConfig.getPlanningApiAddress());

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer slaTemplateOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLATemplate-WebPool.xml"));

        SupplyChainManager supplyChainManager = new SupplyChainManager(
                appConfig.getServiceManagerApiAddress(),
                appConfig.getPlanningApiAddress());
        System.out.println("Generating supply chains...");
        List<SupplyChain> supplyChains = supplyChainManager.buildSupplyChains(slaTemplateOffer);
        System.out.println("Supply chains returned by the Supply chain manager:");
        System.out.println(JsonDumper.dump(supplyChains));
        System.out.println();

        SupplyChain selectedSupplyChain = supplyChains.get(0);
        System.out.println("Selected supply chain id: " + selectedSupplyChain.getId());

        // create planning activity at Planning
        System.out.println("Creating planning activity at the Planning...");
        Map<String, String> data = new HashMap<String, String>();
        data.put("supply_chain_id", selectedSupplyChain.getId());
        PlanningActivity planningActivity = planningApiTarget
                .path("/plan-activities")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(data), PlanningActivity.class);

        System.out.println("Planning activity returned by the Planning:");
        System.out.println(JsonDumper.dump(planningActivity));
        System.out.println();

    }

}
