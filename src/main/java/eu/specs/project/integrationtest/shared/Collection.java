package eu.specs.project.integrationtest.shared;

import eu.specs.datamodel.agreement.offer.AgreementOffer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adispataru on 7/2/15.
 */
@XmlRootElement
@XmlType(propOrder = {"itemList"})
public class Collection<T>{

    @XmlAttribute
    public String resource;
    @XmlAttribute
    public Long total;
    @XmlAttribute
    public Long items;

    private List<String> itemList;

    public List<String> getItemList() {
        if (itemList == null)
            itemList = new ArrayList<String>();
        return itemList;
    }

    public void setItemList(List<String> itemList) {
        this.itemList = itemList;
    }

    public void fromList(List<T> list, String baseURL){
        this.itemList = new ArrayList<String>();
        //TODO baseURL setup
        for (T element : list){
            if (element instanceof AgreementOffer){
                String id =  ((AgreementOffer) element).getName();
                itemList.add(baseURL + "/" + resource + "/" + id);
            }else if(element instanceof String){
                itemList.add(baseURL + "/" + resource + "/" + element);
            }
            else
                itemList.add(baseURL + "/" + resource);
        }
        this.items = (long) list.size();
        this.total = (long) list.size();
    }
}
