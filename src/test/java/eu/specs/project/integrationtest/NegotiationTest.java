package eu.specs.project.integrationtest;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.shared.Collection;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@Ignore
public class NegotiationTest {
    private static final String SLA_TEMPLATE_ID = "WebPool";
    private static final String SLA_TEMPLATE_FILE = "/SLATemplate-WebPool.xml";
    private static AppConfig appConfig;

    @Before
    public void setUp() throws Exception {
        appConfig = new AppConfig("/app.properties");
    }

    @Test
    public void test() throws Exception {

        //Read template from resources
        String xml = readStream(this.getClass().getResourceAsStream(SLA_TEMPLATE_FILE));
        //Register template in SLOManager
        RestTemplate restClient = new RestTemplate();
        restClient.getMessageConverters().add(new Jaxb2RootElementHttpMessageConverter());
        restClient.getMessageConverters().add(new StringHttpMessageConverter());


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(org.springframework.http.MediaType.TEXT_XML);

        HttpEntity<String> request = new HttpEntity<String>(
                xml, headers);
        //Delete template if existing
        restClient.delete(appConfig.getSloManagerAddress() + "/sla-templates/" + SLA_TEMPLATE_ID);
        String xmlURI = restClient.postForObject(appConfig.getSloManagerAddress() + "/sla-templates", request, String.class);
        xmlURI = xmlURI.replace("\"", "");
        assertNotNull(xmlURI);

        //Get template identifier
        String templateId = null;
        String[] tokens = xmlURI.split("/");
        templateId = tokens[tokens.length - 1];
        assertNotNull(templateId);

        /**
         * Based on the negotiation diagram, here starts the negotiation.
         * When a GET is performed on the template id, SLOManager will create a new entry for that template in the
         * SLAManager. The <wsag:Name> of the returned document will contain at the end the id of the created
         * SLATemplate in SLAManager.
         */
        AgreementOffer template = restClient.getForObject(xmlURI, AgreementOffer.class);



        assertNotNull(template);


        //Retrieve the collection of SLAOffers for the received template
        HttpHeaders offerHeaders = new HttpHeaders();
        offerHeaders.setContentType(MediaType.TEXT_XML);
        List<MediaType> accepts = new ArrayList<MediaType>();
        accepts.add(MediaType.TEXT_XML);
        offerHeaders.setAccept(accepts);
        HttpEntity<AgreementOffer> offerRequest = new HttpEntity<AgreementOffer>(template, offerHeaders);
        //String offers = restClient.postForObject(xmlURI + "/slaoffers", offerRequest, String.class);
        System.out.println(xmlURI);
        String offerPath = appConfig.getSloManagerAddress() + "/sla-templates/" + template.getName();
        System.out.println(offerPath);
        Collection<String> offers = restClient.postForObject(offerPath + "/slaoffers", offerRequest, Collection.class);

        System.out.println("------OFFERS--------");
        for(String o : offers.getItemList()){
            System.out.println(o);
        }

        //Retrieve the first SLAOffer.
        String offerURI = (String) ((Collection)offers).getItemList().get(0);
        AgreementOffer offer = restClient.getForObject(offerURI, AgreementOffer.class);
        assertNotNull(offer);

        //Submit the first offer as selected offer.
        restClient.put(offerPath + "/slaoffers/current", offer, AgreementOffer.class);


        //Retrieve the current offer.
        AgreementOffer selectedOffer = restClient.getForObject(offerPath + "/slaoffers/current", AgreementOffer.class);

        //Check whether the submitted and the current SLAOffer are the same.
        assertEquals(selectedOffer.getName(), offer.getName());

        System.out.println("Negotiation Integration test finished successfully.");
    }

    public static String readStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader r = new InputStreamReader(is, "UTF-8");
            int c = 0;
            while ((c = r.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return sb.toString();
    }
}
