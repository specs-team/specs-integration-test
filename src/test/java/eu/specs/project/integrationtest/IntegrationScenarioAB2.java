package eu.specs.project.integrationtest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.integrationtest.shared.CollectionType;
import eu.specs.project.integrationtest.utils.AppConfig;
import eu.specs.project.integrationtest.utils.JsonDumper;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class IntegrationScenarioAB2 {
    private static AppConfig appConfig;

    @Before
    public void setUp() throws IOException {
        appConfig = new AppConfig("/app.properties");
    }

    @Test
    public void testScenario() throws Exception {
        System.out.println("Running integration scenario AB2...");
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                        //.register(new LoggingFilter())
                .build();
        WebTarget sloManagerTarget = client.target(appConfig.getSloManagerAddress());
        WebTarget planningTarget = client.target(appConfig.getPlanningApiAddress());
        WebTarget implementationTarget = client.target(appConfig.getImplementationApiAddress());

        String slaTemplateXml = IOUtils.toString(
                this.getClass().getClassLoader().getResourceAsStream(appConfig.getSlaTemplateFileName()));

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer slaTemplate = (AgreementOffer) u.unmarshal(new StringReader(slaTemplateXml));

        System.out.println("Removing old SLA template if it exists...");
        sloManagerTarget
                .path("/sla-templates/{name}")
                .resolveTemplate("name", slaTemplate.getName())
                .request()
                .delete();

        System.out.println(String.format("Uploading SLA template %s...", slaTemplate.getName()));
        sloManagerTarget
                .path("/sla-templates")
                .request()
                .post(Entity.entity(slaTemplateXml, MediaType.TEXT_XML_TYPE));

        System.out.println("Retrieving custom SLA 1...");
        String customSla1Xml = sloManagerTarget
                .path("/sla-templates/{name}")
                .resolveTemplate("name", slaTemplate.getName())
                .request(MediaType.TEXT_XML)
                .get(String.class);

        System.out.println("Retrieving custom SLA 2...");
        String customSla2Xml = sloManagerTarget
                .path("/sla-templates/{name}")
                .resolveTemplate("name", slaTemplate.getName())
                .request(MediaType.TEXT_XML)
                .post(Entity.entity(customSla1Xml, MediaType.TEXT_XML_TYPE), String.class);

        AgreementOffer customSla = (AgreementOffer) u.unmarshal(new StringReader(customSla2Xml));
        System.out.println(String.format("Custom SLA %s:", customSla.getName()));
        System.out.println(customSla2Xml);

        // get SLA offers
        CollectionType slaOffersCollection = sloManagerTarget
                .path("/sla-templates/{slatOfferId}/slaoffers")
                .resolveTemplate("slatOfferId", customSla.getName())
                .request("text/xml")
                .post(Entity.entity(customSla2Xml, MediaType.TEXT_XML_TYPE), CollectionType.class);

        System.out.println("SLA offers collection:");
        System.out.println(JsonDumper.dump(slaOffersCollection));

        if (slaOffersCollection.getItemList().size() == 0) {
            throw new Exception("No SLA offers available.");
        }

        // retrieve supply chains collection
        ResourceCollection supplyChainCollection = planningTarget
                .path("/supply-chains")
                .queryParam("slaId", customSla.getName())
                .request(MediaType.APPLICATION_JSON)
                .get(ResourceCollection.class);

        System.out.println("Supply chains collection:");
        System.out.println(JsonDumper.dump(supplyChainCollection));
        System.out.println();

        // retrieve supply chains
        for (ResourceCollection.Item item : supplyChainCollection.getItemList()) {
            SupplyChain supplyChain = client
                    .target(item.getItem())
                    .request(MediaType.APPLICATION_JSON)
                    .get(SupplyChain.class);

            System.out.println(String.format("Supply chain %s:", supplyChain.getId()));
            System.out.println(JsonDumper.dump(supplyChain));
            System.out.println();
        }

        // retrieve SLA offer that should be accepted
        System.out.println("SLA offer to accept: " + appConfig.getAcceptedOfferNum());
        String acceptedOfferUri = slaOffersCollection.getItemList().get(appConfig.getAcceptedOfferNum()).getValue();
        System.out.println("Retrieving SLA offer " + acceptedOfferUri);
        String slaOffer1Xml = client
                .target(acceptedOfferUri)
                .request(MediaType.TEXT_XML)
                .get(String.class);
        AgreementOffer slaOffer1 = (AgreementOffer) u.unmarshal(new StringReader(slaOffer1Xml));
        System.out.println(String.format("SLA offer %d (ID=%s):", appConfig.getAcceptedOfferNum(), slaOffer1.getName()));
        System.out.println(slaOffer1Xml);
        System.out.println();

        // accept one of SLA offers (other SLA offers and supply chains will be deleted)
        String slaOffer1Xml1 = sloManagerTarget
                .path("/sla-templates/{slatOfferId}/slaoffers/current")
                .resolveTemplate("slatOfferId", customSla.getName())
                .request(MediaType.APPLICATION_XML_TYPE)
                .put(Entity.entity(slaOffer1Xml, MediaType.TEXT_XML_TYPE), String.class);

        System.out.println(String.format("SLA offer %s has been accepted.", slaOffer1.getName()));
        System.out.println();

        // create planning activity
        System.out.println("Creating planning activity from the SLA template " + customSla.getName());
        Map<String, String> planningActivityData = new HashMap<String, String>();
        planningActivityData.put("sla_id", customSla.getName());
        PlanningActivity planningActivity = planningTarget
                .path("/plan-activities")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(planningActivityData), PlanningActivity.class);

        System.out.println("Planning activity created:");
        System.out.println(JsonDumper.dump(planningActivity));
        System.out.println();

        // retrieve implementation plan
        System.out.println(String.format("Retrieving implementation plan %s from the Implementation...", planningActivity.getActivePlanId()));
        ImplementationPlan implPlan = implementationTarget
                .path("/impl-plans/{implPlanId}")
                .resolveTemplate("implPlanId", planningActivity.getActivePlanId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplementationPlan.class);

        System.out.println("Implementation plan:");
        System.out.println(JsonDumper.dump(implPlan));
        System.out.println();

        // retrieve implementation activity
        System.out.println(String.format("Retrieving implementation activity %s from the Implementation...", planningActivity.getImplActivityId()));
        ImplActivity implActivity = implementationTarget
                .path("/impl-activities/{implActId}")
                .resolveTemplate("implActId", planningActivity.getImplActivityId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplActivity.class);

        System.out.println("Implementation activity:");
        System.out.println(JsonDumper.dump(implActivity));
        System.out.println();

        // wait until the implementation activity finishes
        System.out.println("Waiting until the implementation activity finishes...");
        Date startTime = new Date();
        while (true) {
            // retrieve implementation activity status
            Map<String, String> statusResponse = implementationTarget
                    .path("/impl-activities/{implActId}/state")
                    .resolveTemplate("implActId", planningActivity.getImplActivityId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<Map<String, String>>() {});
            String implActivityState = statusResponse.get("state");

            if (implActivityState.equals(ImplActivity.Status.CREATED.name()) ||
                    implActivityState.equals(ImplActivity.Status.IMPLEMENTING.name())) {
                Thread.sleep(15000);
            } else {
                System.out.println(String.format("Implementation activity finished in %f seconds.",
                        (new Date().getTime() - startTime.getTime())/1000.0));
                System.out.println("Implementation activity state: " + implActivityState);
                System.out.println();
                break;
            }

            if (new Date().getTime() - startTime.getTime() > 300*1000) {
                throw new Exception("Timeout waiting for the implementation activity to finish.");
            }
        }

        // retrieve implementation activity again
        System.out.println(String.format("Retrieving final implementation activity %s...", planningActivity.getImplActivityId()));
        ImplActivity implActivity1 = implementationTarget
                .path("/impl-activities/{implActId}")
                .resolveTemplate("implActId", planningActivity.getImplActivityId())
                .request(MediaType.APPLICATION_JSON)
                .get(ImplActivity.class);

        System.out.println("Implementation activity:");
        System.out.println(JsonDumper.dump(implActivity1));
        System.out.println();

        System.out.println("Integration scenario AB2 finished successfully.");
    }
}
